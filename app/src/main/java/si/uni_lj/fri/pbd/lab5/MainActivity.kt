package si.uni_lj.fri.pbd.lab5

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import si.uni_lj.fri.pbd.lab5.TimerService.Companion.ACTION_START
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity() {
    companion object {
        val TAG = MainActivity::class.simpleName
        // TODO: Uncomment to get periodic UI updates
        // Message type for the handler
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L
    }

    // TODO: Create timerService and serviceBound
    var timerService: TimerService? = null
    var serviceBound: Boolean = false


    // Handler to update the UI every second when the timer is running
    // TODO: Uncomment to get periodic UI updates
    private val updateTimeHandler = object : Handler(Looper.getMainLooper()){
        override fun handleMessage(message: Message) {
            if (MSG_UPDATE_TIME == message.what) {
                Log.d(TAG, "updating time")
                updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
            }
        }
    }


    // TODO: Uncomment to define a ServiceConnection
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "Service bound")
            val binder = iBinder as TimerService.RunServiceBinder
            timerService = binder.service
            serviceBound = true
            timerService?.background()
            // Update the UI if the service is already running the timer
            if (timerService?.isTimerRunning == true) {
                updateUIStartRun()
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(TAG, "Service disconnect")
            serviceBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onStart() {
        super.onStart()

        // TODO: uncomment this and write the code to the Service
        Log.d(TAG, "Starting and binding service")

        val i = Intent(this, TimerService::class.java)
        i.action = ACTION_START
        startService(i)

        // TODO: then uncomment this to bind the Service
        bindService(i, mConnection, 0);
    }


    override fun onStop() {
        super.onStop()
        updateUIStopRun()

        // TODO: if the Service is bound, unbind it

        if(serviceBound) {

            if(timerService?.isTimerRunning!!) {
                timerService?.foreground()
            }
            else {
                stopService(Intent(this, TimerService::class.java))
            }

            //unbindService(mConnection)
            //serviceBound = false
        }
    }

    fun runButtonClick(v: View?) {

        // TODO: modify to check whether the service is bound and whether the service's timer is running
        //  and then start/stop the service's timer

        if(serviceBound){
            if(!timerService?.isTimerRunning!!) {
                timerService?.startTimer()
                updateUIStartRun()
            }
            else {
                timerService?.stopTimer()
                updateUIStopRun()
            }
        }
    }

    /**
     * Updates the UI when a run starts
     */
    private fun updateUIStartRun() {
        // TODO: Uncomment to start periodic UI updates
        updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
        timer_button.text = getString(R.string.timer_stop_button)
    }

    /**
     * Updates the UI when a run stops
     */
    private fun updateUIStopRun() {
        // TODO: Uncomment to stop periodic UI updates
        updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
        timer_button.text = getString(R.string.timer_start_button)
    }

    /**
     * Updates the timer readout in the UI; the service must be bound
     */
    private fun updateUITimer() {

        // TODO: check if the Service is bound and set the text view

        if(serviceBound) {
            timer_text_view.text = timerService?.elapsedTime().toString() + "seconds"
        }
    }


    /**
     * When the timer is running, use this handler to update
     * the UI every second to show timer progress
     */
    // TODO: Uncomment the handler to get periodic UI updates
    /*internal class UIUpdateHandler(activity: MainActivity) : Handler() {
        private val activity: WeakReference<MainActivity>
        override fun handleMessage(message: Message) {
            if (MSG_UPDATE_TIME == message.what) {
                Log.d(TAG, "updating time")
                activity.get()!!.updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS.toLong())
            }
        }

        companion object {
            private const val UPDATE_RATE_MS = 1000
        }

        init {
            this.activity = WeakReference(activity)
        }
    }*/

}